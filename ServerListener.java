import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerListener implements iListener, Runnable{
	
	Socket s;
	BufferedReader input;
	PrintWriter output;
	iServer serverApp;
	
	public ServerListener(Socket s, BufferedReader input, PrintWriter output, iServer serverApp){
		this.s = s;
		this.input = input;
		this.output = output;
		this.serverApp = serverApp;
	}
	
	@Override
	public String atender(String mensaje) {
		System.out.println("Recibi: " + mensaje);
		String accion = mensaje.split("/")[0];
		String parametros = mensaje.split("/", 2)[1];
		String respuesta = "ERROR";
		switch (accion) {
		
		case "bnpr":
			//buscar nodo por recurso
			respuesta = serverApp.buscarNodoPorRecurso(parametros);
			break;
		
		case "reg":
			//registrarse
			respuesta = serverApp.registrar(parametros);
			break;
		
		case "b":
			//darse de baja
			respuesta = serverApp.baja(parametros);
			break;
			
		default:
			break;
		}
		return respuesta;
	}

	@Override
	public void run() {
		try {
			String peticion = input.readLine();
			String respuesta = this.atender(peticion);
			this.output.println(respuesta);
			
			this.input.close();
			this.output.close();
			this.s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
