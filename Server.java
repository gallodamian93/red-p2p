import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server implements iPuntoDeEntrada{
	iServer serverApp;
	ServerConf myConf;
	
	public Server(iServer serverApp, ServerConf myConf){
		this.serverApp = serverApp;
		this.myConf = myConf;
	}
	
	public void iniciar(){
		try {
			ServerSocket ss = new ServerSocket(myConf.port);
			System.out.println("[SERVER] Iniciado en puerto " + myConf.port);
			while (true){
				Socket s = ss.accept();
				System.out.println("[SERVER] Server o nodo conectado");
				BufferedReader input = new BufferedReader (new InputStreamReader (s.getInputStream()));
				PrintWriter output = new PrintWriter (new OutputStreamWriter (s.getOutputStream()),true);
				ServerListener sl = new ServerListener(s, input, output, this.serverApp);
				Thread slThread = new Thread (sl);
				slThread.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
