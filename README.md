## Sobre el proyecto

Red P2P de carga, búsqueda y descarga de archivos que sigue las
siguientes pautas:

- Existen dos tipos de nodos, Maestros y Extremos. Los primeros, son servidores
  centralizados replicados (al menos 2 nodos) que disponen del listado actualizado
  de los nodos extremos y se encargan de gestionar la E/S de los peers. Los
  segundos cumplen dos funciones en el sistema: realizan consultas (como
  clientes) y atienden solicitudes (como servidores).
- Funcionamiento:
  -- Cada extremo dispone de las direcciones IP de los nodos Maestros. Al iniciarse se
  contacta con un maestro el cual funciona como punto de acceso al
  sistema e informa cuáles son los archivos que dispone para
  compartir. Luego, está atento a trabajar en dos modos (cliente y
  servidor)
  -- Como cliente, deriva consultas al nodo maestro y una vez obtenida la
  respuesta, seleccionará el/los recursos que desee descargar y se contactará con el par correspondiente para descargar el/los archivo/s.
  -- Como servidor, recibe la consulta, revisa si matchea la consulta con alguno de
  los recursos disponibles y devuelve los resultados al nodo que
  solicitó resultados.

## Instrucciones

1. Correr MasterServerAPP.java
2. Correr SlaveServerAPP.java
3. Correr NodoAPP.java: por defecto la rutina que tiene es la de registrarse y, posteriormente, darse de baja. En la consola se puede ver el intercambio de mensajes entre el nodo y los servers. Dejé comentado la rutina para pedir un recurso. Para implementarlo simplemente hay que levantar un nodo que cuando se registre envíe los recursos que tiene, y luego levantar otro que lo pida.

