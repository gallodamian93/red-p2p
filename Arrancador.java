
public class Arrancador implements Runnable{
	private iPuntoDeEntrada server;
	
	public Arrancador(iPuntoDeEntrada server){
		this.server = server;
	}
	
	@Override
	public void run() {
		this.server.iniciar();
		
	}

}
