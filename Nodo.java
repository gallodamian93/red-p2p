import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Nodo implements iPuntoDeEntrada{
	NodoAPP nodoApp;
	
	public Nodo (NodoAPP nodoApp){
		this.nodoApp = nodoApp;
	}
	
	/**
	 * Metodo de entrada del nodo.
	 * Todos los que se quieran comunicar con este nodo se conectan a traves de 
	 * este socket
	 */
	public void iniciar(){
		int port = Integer.parseInt(nodoApp.mySocket.split(":")[1]);
		try {
			ServerSocket ss = new ServerSocket(port);
			System.out.println("[NODO] Iniciado en puerto " + port);
			while (true){
				Socket s = ss.accept();
				System.out.println("[NODO] Nodo conectado");
				BufferedReader input = new BufferedReader (new InputStreamReader (s.getInputStream()));
				PrintWriter output = new PrintWriter (new OutputStreamWriter (s.getOutputStream()),true);
				NodoListener nl = new NodoListener (s, input, output, this.nodoApp);
				Thread smThread = new Thread (nl);
				smThread.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
