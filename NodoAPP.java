import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class NodoAPP implements iNodo, iServer{
	ArrayList<ServerConf> servidores;
	ArrayList<Recurso> recursos;
	String mySocket;
	
	public NodoAPP(ArrayList<ServerConf> servidores, ArrayList<Recurso> recursos, String mySocket){
		this.servidores = servidores;
		this.recursos = recursos;
		this.mySocket = mySocket;
	}
	
	/**
	 * Metodo privado que envia String al socket y devuelve la respuesta
	 * @param peticion
	 * @param socket
	 * @return
	 */
	private String send(String peticion, String socket){
		String respuesta = "";
		String ip = socket.split(":")[0];
		int port = Integer.parseInt(socket.split(":")[1]);
		try {
			Socket s = new Socket(ip, port);
			BufferedReader input = new BufferedReader (new InputStreamReader (s.getInputStream()));
			PrintWriter output = new PrintWriter (new OutputStreamWriter (s.getOutputStream()),true);
			output.println(peticion);
			System.out.println("[NODO] Mensaje enviado("  + peticion + ") a " + ip + ":" + port);
			respuesta = input.readLine();
			
			input.close();
			output.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return respuesta;
	}
	
	/**
	 * Envia peticion al servidor para solicitar el socket del nodo que tiene un recurso
	 */
	@Override
	public String buscarNodoPorRecurso(String nombreRecurso) {
		//genero peticion
		String peticion = "bnpr/" + nombreRecurso;
		//selecciono servidor random
		Random rand = new Random();
		int x = rand.nextInt(this.servidores.size());
		ServerConf server = this.servidores.get(x);
		String socket = server.ip + ":" + server.port;
		//envio peticion
		String respuesta = this.send(peticion, socket);
		return respuesta;
	}
	
	/**
	 * Envia peticion al servidor para registrarse
	 * @param nodo
	 * @return
	 */
	@Override
	public String registrar(String nodo) {
		//genero string de mis recursos en formato "nombreRecurso1-nombreRecurso2-nombreRecurso3"
		StringBuilder StringRec = new StringBuilder();
		for (Recurso recurso : recursos) {
			StringRec.append(recurso.nombre);
			//si no es el ultimo le agrego un guion al string
			if (!(Integer.compare(recursos.indexOf(recurso), recursos.size()-1) == 0)){
				StringRec.append("-");
			}
		}
		String myRec = StringRec.toString();
		//genero peticion
		String peticion = "reg/" + this.mySocket + "," + myRec;
		//selecciono servidor random
		Random rand = new Random();
		int x = rand.nextInt(this.servidores.size());
		ServerConf server = this.servidores.get(x);
		String socket = server.ip + ":" + server.port;
		//envio peticion
		String respuesta = this.send(peticion, socket);
		return respuesta;
	}
	
	/**
	 * Este nodo envia peticion de baja al servidor
	 */
	@Override
	public String baja(String nodo) {
		//genero peticion
		String peticion = "b/" + this.mySocket;
		//selecciono servidor random
		Random rand = new Random();
		int x = rand.nextInt(this.servidores.size());
		ServerConf server = this.servidores.get(x);
		String socket = server.ip + ":" + server.port;
		//envio peticion
		String respuesta = this.send(peticion, socket);
		return respuesta;
	}
	
	/**
	 * Envia peticion a otro nodo para solicitar un recurso
	 */
	@Override
	public String pedirRecurso(String recurso, String socketNodo) {
		//genero peticion
		String peticion = "gr/" + recurso;
		//envio la peticion
		String respuesta = this.send(peticion, socketNodo);
		return respuesta;
	}
	
	/**
	 * Devuelve String vacio si no existe el recurso
	 * y si existe devuelve "nombreRecurso:dato".
	 * Este metodo se ejecuta cuando otro nodo ejecuta PedirRecurso
	 */
	public String getRecurso(String nombreRecurso){
		String respuesta = "";
		for (Recurso recurso : recursos) {
			if(recurso.nombre.equals(nombreRecurso)){
				respuesta = recurso.nombre + ":" + recurso.dato;
				break;
			}
		}
		return respuesta;
	}
	
	public static void main(String[] args) throws InterruptedException {

		//configuracion inicial del nodo
		ArrayList<Recurso> recursos = new ArrayList<Recurso>();
		//Recurso rec = new Recurso ("A", "Este es el recurso A");
		//recursos.add(rec);
		
		ServerConf master = new ServerConf("127.0.0.1" , 15000);
		ServerConf slave = new ServerConf("127.0.0.1" , 15001);
		
		ArrayList<ServerConf> servidores = new ArrayList<ServerConf>();
		servidores.add(master);
		servidores.add(slave);
		
		//levanto nodo
		NodoAPP nodoApp = new NodoAPP(servidores, recursos, "127.0.0.1:16002");
		Nodo nodo = new Nodo(nodoApp);
		Arrancador init = new Arrancador(nodo);
		Thread initThread = new Thread(init);
		initThread.start();
		
		//acciones que realiza el nodo
		
		System.out.println(nodoApp.registrar(""));
		System.out.println(nodoApp.baja(""));
		
		//para pedir un recurso de otro nodo:
		//System.out.println(nodoApp.pedirRecurso("A", nodoApp.buscarNodoPorRecurso("A")));
		
		
	}
	
}
