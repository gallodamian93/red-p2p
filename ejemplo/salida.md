## Inicialización de servidores y registro de un nodo

Rutina especificada en el archivo *readme.md*.

MasterServerAPP:

`[SERVER] Iniciado en puerto 15000`

`[SERVER] Server o nodo conectado
Recibi: reg/127.0.0.1:16002,
Agregando recursos al cliente: null`

`[MASTER SERVER] Agregado cliente 127.0.0.1:16002(recursos: )`

`[MASTER SERVER] Mensaje 'reg/m/127.0.0.1:16002,' enviado a SLAVE`

SlaveServerAPP:

`[SERVER] Iniciado en puerto 15001`

`[SERVER] Server o nodo conectado
Recibi: reg/127.0.0.1:16002,`

`[SLAVE SERVER] Mensaje 'reg/127.0.0.1:16002,' enviado a MASTER`

`[SERVER] Server o nodo conectado
Recibi: reg/m/127.0.0.1:16002,`

NodoAPP:

`[NODO] Iniciado en puerto 16002`

`[NODO] Mensaje enviado(reg/127.0.0.1:16002,) a 127.0.0.1:15001`