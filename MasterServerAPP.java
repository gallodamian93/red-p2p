import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public class MasterServerAPP extends ServerAPP implements iServer{

	ArrayList<ServerConf> servidores;
	ArrayList<String> errores;
	
	public MasterServerAPP(ArrayList<ServerConf> servidores, ServerConf myConf) {
		super(myConf);
		this.servidores = servidores;
		this.errores = new ArrayList<String>();
	}


	@Override
	public String registrar(String nodo) {
		synchronized(this.clientes){
			String respuesta;
			//primero registra en su serverApp
			try {
				String socket = nodo.split(",")[0];
				String recString;
				String[] recursos;
				if(nodo.split(",").length > 1){
					recString = nodo.split(",")[1];
					recursos = recString.split("-");
				} else {
					recursos = null;
				}
	
				Cliente registrado = new Cliente();
				registrado.socket = socket;
				System.out.println("Agregando recursos al cliente: " + recursos);
				if(!(recursos == null)){
					for (String rec : recursos) {
						registrado.recursos.add(new Recurso(rec));
					}
				}
				this.clientes.add(registrado);
				System.out.println("[MASTER SERVER] Agregado cliente " + registrado.socket + "(recursos: " + registrado.listarRecursos() + ")");
				
				//luego replica
				String peticion = "reg/m/" + nodo;
				for (ServerConf servidor : servidores) {
					String socketServ = servidor.ip + ":" + servidor.port;
					respuesta = this.replicar(socketServ , peticion);
					if (respuesta.equals("ERROR")) {
						errores.add("SLAVE: " + socketServ + "MSG: " + peticion );
					}
				}
				respuesta = "OK";
			} catch (Exception e) {
				respuesta = "ERROR";
			}
	
			return respuesta;
		}
	}
	
	@Override
	public String baja(String nodo) {
			String respuesta = "ERROR";
			//primero da de baja en su serverApp
			try {
				Integer index = null;
				for (Cliente cliente : clientes) {
					if (cliente.socket.equals(nodo)) {
						index = clientes.indexOf(cliente);
						break;
					}
				}
				if (!(index == null)) {
					//existe cliente, voy a borrarlo
					clientes.remove(index.intValue());
					System.out.println("[MASTER SERVER] Cliente eliminado!");
					System.out.println("Estado actual de Lista de Clientes: ");
					for (Cliente cliente : clientes) {
						System.out.print(cliente.socket + ",");
					}
					System.out.println();
					
				}
				
				//luego replica
				String peticion = "b/m/" + nodo;
				for (ServerConf servidor : servidores) {
					String socketServ = servidor.ip + ":" + servidor.port;
					respuesta = this.replicar(socketServ , peticion);
					if (respuesta.equals("ERROR")) {
						errores.add("SLAVE: " + socketServ + "MSG: " + peticion );
					}
				}
				respuesta = "OK";
			} catch (Exception e) {
				respuesta = "ERROR";
			}
	
			return respuesta;
		
	}

	public String replicar(String socket, String mensaje){
		String respuesta;
		try {
			Socket s = new Socket(socket.split(":")[0], Integer.parseInt(socket.split(":")[1]));
			BufferedReader input = new BufferedReader (new InputStreamReader (s.getInputStream()));
			PrintWriter output = new PrintWriter (new OutputStreamWriter (s.getOutputStream()),true);
			output.println(mensaje);
			System.out.println("[MASTER SERVER] Mensaje '"+ mensaje +"' enviado a SLAVE");
			respuesta = input.readLine();
			
			input.close();
			output.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
			respuesta = "ERROR";
		}
		return respuesta;
	}

	
	public static void main(String[] args) throws InterruptedException {
		//configuracion inicial
		ServerConf master = new ServerConf("127.0.0.1" , 15000);
		ServerConf slave = new ServerConf("127.0.0.1" , 15001);
		
		ArrayList<ServerConf> slaves = new ArrayList<ServerConf>();
		slaves.add(slave);
		
		//levanto el servidor
		MasterServerAPP masterApp = new MasterServerAPP(slaves, master);
		Server masterSV = new Server(masterApp, master);
		masterSV.iniciar();
		

	}
}
