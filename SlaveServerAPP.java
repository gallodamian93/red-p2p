import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public class SlaveServerAPP extends ServerAPP implements iServer{
	ServerConf master;
	
	public SlaveServerAPP(ServerConf myConf, ServerConf master) {
		super(myConf);
		this.master = master;	
	}

	public String repliRegistrar(String nodo){
		synchronized(this.clientes){
			String respuesta = "ERROR";
			try {
				String socket = nodo.split(",")[0];
				String recString;
				String[] recursos;
				if(nodo.split(",").length > 1){
					recString = nodo.split(",")[1];
					recursos = recString.split("-");
				} else {
					recursos = null;
				}
				Cliente registrado = new Cliente();
				registrado.socket = socket;
				System.out.println("Agregando recursos al cliente: " + recursos);
				if(!(recursos == null)){
					for (String rec : recursos) {
						registrado.recursos.add(new Recurso(rec));
					}
				}
				this.clientes.add(registrado);
				System.out.println("[SLAVE SERVER] Agregado cliente " + registrado.socket + "(recursos: " + registrado.listarRecursos() + ")");
				respuesta = "OK";
			} catch (Exception e) {
				respuesta = "ERROR";
			}
			
			return respuesta;
		}
	}
	
	private String repliBaja(String socket) {
		synchronized(this.clientes){
			String respuesta = "ERROR";
			Integer index = null;
			for (Cliente cliente : clientes) {
				if (cliente.socket.equals(socket)) {
					index = clientes.indexOf(cliente);
					break;
				}
			}
			if (!(index == null)) {
				//existe cliente, voy a borrarlo
				clientes.remove(index.intValue());
				System.out.println("[SLAVE SERVER] Cliente eliminado!");
				System.out.println("Estado actual de Lista de Clientes: ");
				for (Cliente cliente : clientes) {
					System.out.print(cliente.socket + ",");
				}
				System.out.println();
			}
			respuesta = "OK";
			return respuesta;
		}
	}
	
	/**
	 * si parametro = "/m/etc..." entonces es una replicacion, sino le mando al master
	 */
	@Override
	public synchronized String registrar(String nodo) {
		String respuesta = "ERROR";
		if (this.esReplicacion(nodo)) {
			System.out.println("Es una replicacion!");
			respuesta = this.repliRegistrar(nodo.split("/")[1]);
		} else {
			//un cliente me mando, por lo que debo mandarselo al master
			String peticion = "reg/" + nodo;
			respuesta = this.send(peticion);
		}
		return respuesta;
	}
	
	public boolean esReplicacion(String mensaje){
		if(mensaje.split("/").length > 1){
			if (mensaje.split("/")[0].equals("m")){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String baja(String nodo) {
		String respuesta = "ERROR";
		if (this.esReplicacion(nodo)) {
			System.out.println("Es una replicacion!");
			respuesta = this.repliBaja(nodo.split("/")[1]);
		} else{
			String peticion = "b/" + nodo;
			respuesta = this.send(peticion);
		}
			
		return respuesta;
	}
	


	/**
	 * Metodo privado que envia String al socket y devuelve la respuesta
	 * @param peticion
	 * @param socket
	 * @return
	 */
	private String send(String peticion){
		String respuesta;
		try {
			Socket s = new Socket(master.ip, master.port);
			BufferedReader input = new BufferedReader (new InputStreamReader (s.getInputStream()));
			PrintWriter output = new PrintWriter (new OutputStreamWriter (s.getOutputStream()),true);
			output.println(peticion);
			System.out.println("[SLAVE SERVER] Mensaje '"+ peticion + "' enviado a MASTER");
			respuesta = input.readLine();
			
			input.close();
			output.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
			respuesta = "ERROR";
		}
		return respuesta;
	}
	
	public static void main(String[] args) throws InterruptedException {
		//configuracion inicial
		ServerConf master = new ServerConf("127.0.0.1" , 15000);
		ServerConf slave = new ServerConf("127.0.0.1" , 15001);
		
		//levanto el server
		SlaveServerAPP slaveApp = new SlaveServerAPP(slave, master);
		Server slaveSV = new Server(slaveApp, slave);
		slaveSV.iniciar();
	}
}
