import java.util.ArrayList;

public class Cliente {
	String socket;
	ArrayList<Recurso> recursos;
	
	public Cliente(){
		this.recursos = new ArrayList<>();
	}
	
	public String listarRecursos(){
		String respuesta = "";
		for (Recurso recurso : recursos) {
			respuesta += recurso.nombre;
			if (!(recursos.indexOf(recurso) == (recursos.size()-1))) {
				respuesta += ", ";
			}
		}
		return respuesta;
	}
}
