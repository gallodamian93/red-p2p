import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class NodoListener implements iListener, Runnable{
	NodoAPP nodoApp;
	Socket s;
	BufferedReader input;
	PrintWriter output;

	public NodoListener(Socket s, BufferedReader input, PrintWriter output, NodoAPP nodoApp) {
		this.s = s;
		this.input = input;
		this.output = output;
		this.nodoApp = nodoApp;
	}

	@Override
	public String atender(String mensaje) {
		//en este caso atender tiene solo 1 metodo a invocar ya que es del nodo: pedirrecurso
		String accion = mensaje.split("/")[0];
		String parametros = mensaje.split("/")[1];
		String respuesta = "ERROR";
		switch (accion) {
		case "gr":
			respuesta = nodoApp.getRecurso(parametros);
			break;

		default:
			break;
		}
		return respuesta;
	}

	@Override
	public void run() {
		try {
			String peticion = input.readLine();
			String respuesta = this.atender(peticion);
			this.output.println(respuesta);
			
			this.input.close();
			this.output.close();
			this.s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
