
public interface iServer {
	String buscarNodoPorRecurso(String nombreRecurso);
	String registrar(String nodo);
	String baja(String nodo);
}
