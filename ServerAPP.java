import java.util.ArrayList;

public class ServerAPP {
	ArrayList<Cliente> clientes;
	ServerConf myconf;
	
	public ServerAPP(ServerConf myConf){
		this.clientes = new ArrayList<Cliente>();
		this.myconf = myConf;
	}
	
	/**
	 * Devuelve vacio si nadie tiene el recurso
	 * Devuelve 1 socket si existe el recurso (el primero que encuentra)
	 * @param buscado
	 * @return
	 */
	public String buscarNodoPorRecurso(String buscado) {
		String respuesta = "ERROR: Nadie dispone del recurso pedido";
		for (Cliente cliente : clientes) {
			for (Recurso rec : cliente.recursos) {
				if (buscado.equals(rec.nombre)) {
					respuesta = cliente.socket;
					break;
				}
			}
		}
		return respuesta;
	}
}
